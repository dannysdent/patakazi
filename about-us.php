
<?php

include "header.php";

?>


                			<div id="tg-innerbanner" class="tg-innerbanner tg-bglight tg-haslayout">
				                <div class="tg-pagebar tg-haslayout opacity-false">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
                                <h1>About Us</h1>
                                							</div>
						</div>
					</div>
				</div>
			</div>
		             	<main id="main" class="tg-page-wrapper tg-haslayout">
            			<div class="container">
				<div class="row">
				<div class="main-page-wrapper tg-haslayout">
				<div class="fw-page-builder-content">    
				<section  class="tg-main-section haslayout  stretch_section_contents_corner stretch_section stretch_data"  style="margin-top:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    
<figure class="frame-img"><img src="wp-content/uploads/2016/02/Task-About-1400x300.jpg" alt="Image Frame" /></figure>
</div>
        </div>
    </section>
    <div class="section-current-width"></div>    <section  class="tg-main-section haslayout  default"  style="margin-top:-100px;margin-bottom:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    
	<div class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="tg-theme-heading">
							<h2>Who We Are</h2>
								</div>
	</div>
									</div>
<div class="col-xs-12 col-md-1 builder-column " >
    </div>
<div class="col-xs-12 col-md-8 builder-column " >
    <p>PataKazi is a technology start-up with a vision of creating happy, stress-free households. PataKazi facilitates reliable, high-quality home services for customers, while driving higher productivity for servicemen.</p><p>We aim to revolutionise home services in Kenya by addressing three of the biggest customer challenges: delays, poor quality and lack of price transparency. PataKazi simplifies the entire process, right from finding a serviceman to ensuring completion of the service.</p>
    <div class="tg-buynowbox">
	<h3 style="color:#5d5955"></h3>
	    	<a class="tg-btn" style="color:#5d5955" href="dir-search.php">Browse Professionals</a>
    </div></div>
<div class="col-xs-12 col-md-1 builder-column " >
    </div>
        </div>
    </section>
    </div></div>				</div>
			</div>
					</main>
                           
<?php

include "footer.php";

?>