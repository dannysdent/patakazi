<?php

include "header.php";

?>

                             	<main id="main" class="tg-page-wrapper tg-haslayout">
            			<div class="container">
				<div class="row">
					<div class="main-page-wrapper tg-haslayout"><div class="fw-page-builder-content">   
           <section  class="tg-main-section haslayout  stretch_section_contents_corner stretch_section stretch_data"  style="margin-top:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    <div class="sc-dir-search v1 sc-dir-search-v1">
  <div id="tg-homebanner" class="tg-homebanner tg-overflowhidden tg-haslayout">s
    <div id="map_canvas" class="tg-location-map tg-haslayout"></div>
            
    <div class="tg-banner-content">
      <div class="tg-displaytable">
        <div class="tg-displaytablecell">
          <div class="container">
            <form class="form-searchdoctors directory-map" id="directory-map">
            <div class="col-md-3 col-sm-5 col-xs-3 tg-verticalmiddle">
              <div class="row">
                                <ul id="tg-sortable" class="tg-tabs-nav tg-sortable" role="tablist">
                                           <li data-dir_name="Carpenter" data-id="123" class="active current-directory">
                         	<input type="radio" checked name="directory_type" id="123" value="123" />
                            <label for="123">
                                <i class="fa fa-legal"></i> <a href="dir-search.php?cat=1">
                                <span class="tg-category"> 
                                    <span class="tg-category-name">Carpenter</span>
                                    
                                </span> </a>
                            </label>
                          </li>
                                          <li data-dir_name="Electrician" data-id="126" class=" current-directory">
                         	<input type="radio"  name="directory_type" id="126" value="126" />
                            <label for="126">
                                <i class="fa fa-flash"></i> <a href="dir-search.php?cat=2"> 
                                <span class="tg-category"> 
                                   <span class="tg-category-name">Electrician</span>
                                    
                                </span> </a>
                            </label>
                          </li>
                                          <li data-dir_name="Plumber" data-id="125" class=" current-directory">
                         	<input type="radio"  name="directory_type" id="125" value="125" />
                            <label for="125">
                                <i class="fa fa-wrench"></i> <a href="dir-search.php?cat=3">
                                <span class="tg-category"> 
                                   <span class="tg-category-name">Plumber</span> 
                                     
                                </span> </a>
                            </label>
                          </li>
                                          <li data-dir_name="Painter" data-id="127" class=" current-directory">
                         	<input type="radio"  name="directory_type" id="127" value="127" />
                            <label for="127">
                                <i class="fa fa-paint-brush"></i> <a href="dir-search.php?cat=4">
                                <span class="tg-category"> 
                                    <span class="tg-category-name">Painter</span>
                                    
                                </span> </a>
                            </label>
                          </li>
                                        
                             <li data-dir_name="Electronics Technician" data-id="2550" class=" current-directory">
                               
                         	<input type="radio"  name="directory_type" id="2550" value="2550" />
                            <label for="2550">

                                <i class="fa fa-calculator"></i>
                                 <a href="dir-search.php?cat=5">
                                <span class="tg-category"> 
                                <span class="tg-category-name">Electronics Technician</span> 
                                   
                                </span> 
                                </a>
                            </label>

                          </li>
                                 </ul>
   
                <script type="text/template" id="tmpl-load-subcategories">
				
				</script> 
                              </div>
            </div>
            <div class="col-md-9 col-sm-7 col-xs-9 tg-verticalmiddle">
              <div class="row">
                  <fieldset>
                    <div class="tab-content tg-tab-content">
                      <div role="tabpanel" class="tab-pane active" id="nephrologist">
                        <div class="row">
                          <div class="col-md-6 col-sm-12 col-xs-12 tg-verticalmiddle">
                            <h1>FIND YOUR<span>NEAREST </span>
                            <em class="current_directory">SPECIALIST</em></h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="show-search"><i class="fa fa-search"></i></div>
  </div>
</div>
</div>
        </div>
    </section>
    <div class="section-current-width"></div>  


    <div class="section-current-width"></div>    <section  class="tg-main-section haslayout  default"  style="margin-top:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    
	<div class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="tg-theme-heading">
							<h2>We Are Here To Help</h2>
								</div>
	</div>
									</div>
<div class="col-xs-12 col-md-12 builder-column " >
    
    <div class="raw-html-description">
        
<div class="table-responsive">
<table class="table wearehere">
<thead>
<tr>
<th class="col-xs-3">Transparency</th>
<th class="col-xs-3">Safety</th>
<th class="col-xs-3">Assurance</th>
<th class="col-xs-3">Chat</th>
</tr>
<tr>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2016/03/icon_green_bulb.png" width="120" height="120" />

We measure and compare market prices to give you a standardized rate</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2016/03/shield-flat.png" width="120" height="120" />

We pre-screen and verify all our servicemen</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2016/03/assurance-icon.png" width="120" height="120" />

We ensure a hassle free process from booking to service delivery</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2017/02/chatIcon-1.png" width="120" height="120" />

For queries, we are glad to have a live chat with you</td>
</tr>
</thead>
</table>
</div>    </div>
</div>
        </div>
    </section>
        <section  class="tg-main-section haslayout  default"  style="margin-top:-100px;margin-bottom:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    
	<div class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="tg-theme-heading">
							<h2>How It Works</h2>
										<span class="tg-roundbox"></span>
				<div class="tg-description">
					<p>PataKazi has made it easier for you to Hire a Trusted Professional in 4 Simple Steps.</p>
				</div>
					</div>
	</div>
									</div>
<div class="col-xs-12 col-md-12 builder-column " >
    
    <div class="raw-html-description">
        <div class="table-responsive">
<table class="table">
<thead>
<tr class="steps">
<th class="col-xs-3"><img src="wp-content/uploads/2017/03/01.png" /></th>
<th class="col-xs-3"><img src="wp-content/uploads/2017/03/22.png" /></th>
<th class="col-xs-3"><img src="wp-content/uploads/2017/03/33.png" /></th>
<th class="col-xs-3"><img src="wp-content/uploads/2017/03/44.png" /></th>
</tr>
<tr>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2017/03/describe.jpg" width="120" height="120" />

<strong>Search for a Technician</strong><br />
Search for a specific technician in Your Desired Category</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2017/03/question.png" width="120" height="120" />

<strong>Browse a list of Professionals</strong><br />
Browse through the database to find your desired technician</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2017/03/calendar-icon.png" width="120" height="120" />

<strong>Contact a Technician </strong><br />
Contact the technician through form or phone number</td>
<td><img class="imghelper aligncenter" src="wp-content/uploads/2017/03/confirm.png" width="120" height="120" />
<strong>Get Work Done</strong><br />
You will get the right technician to do your job</td>
</tr>
</thead>
</table>
</div>    </div>
</div>
        </div>
    </section>
    </div></div>				</div>
			</div>
					</main>

<?php

include "footer.php";

?>