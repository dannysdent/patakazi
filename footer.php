


          
                            <footer id="footer" class="tg-haslayout">
                                            <div class="tg-threecolumn">
                            <div class="container">
                                <div class="row">
                                                                            <div class="col-sm-4">
          <div class="tg-footercol"><div id="nav_menu-2" class="widget_nav_menu tg-widget">
          	<div class="tg-heading-border tg-small"><h4>About Us</h4></div><div class="menu-footer-menu-container">
      <ul id="menu-footer-menu" class="menu">
<li id="menu-item-2238" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2238"><a href="about-us.php">About Us</a></li>
<li id="menu-item-2467" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2467"><a href="#">Privacy Policy</a></li>
<li id="menu-item-2468" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2468"><a href="#">Terms &#038; conditions</a></li>
<li id="menu-item-2239" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2239"><a href="contact-us.php">Contact Us</a></li>
</ul>
</div></div></div>
                                        </div>
                                                                                                                <div class="col-sm-4">
    <div class="tg-footercol"><div id="text-2" class="widget_text tg-widget"><div class="tg-heading-border tg-small"><h4>Our Services</h4></div>			<div class="textwidget"><ul>
<ol><a href="#" target="_blank">Carpentry Services in Kenya</a></ol>
<ol><a href="#" target="_blank">Electrical Services</a></ol>
<ol><a href="#" target="_blank">Plumbing Services</a></ol>
<ol><a href=#" target="_blank">Painting Services in Kenya</a></ol>
<ol><a href="#" target="_blank">Electronics Technician</a></ol>
<ol><a href="#" target="_blank">Air Heating & Conditioning Technician</a></ol>
<ol><a href="#" target="_blank">Project Manager</a></ol>
</ul></div>
		</div></div>
                                        </div>
                                                                                                                <div class="col-sm-4">
                                            <div class="tg-footercol"><div id="address_widget-2" class="address-column"><div class="tg-heading-border tg-small"><h4>Contact Us</h4></div>												<ul class="tg-info">
								<li>
					<i class="fa fa-home"></i>
					<address>Juja Nairobi Kenya, P.O.BOX 57716-00200 Nairobi</address>
				</li>
												<li>
					<i class="fa fa-phone"></i>
					<em><a href="tel:0798639630 / 0770712586">0719579575 / 0727467556</a></em>
				</li>
													<li>
						<i class="fa fa-envelope"></i>
						<em><a href="mailto:info@patakazi.co.ke "?subject=Hello>info@patakazi.co.ke </a></em>
					</li>
								
                				<li>
					<i class="fa fa-fax"></i>
					<em><a href="javascript:;">www.patakazi.co.ke</a></em>
				</li>
							</ul>
						</div></div>
                                        </div>
                                                                    </div>
                            </div>
                        </div>
                                                            <div class="tg-footerbar tg-haslayout">
                        <div class="tg-copyrights">
                            <p>© 2018 PataKazi | All Rights Reserved.</p>
                        </div>
                    </div>
                                    </footer>
                            <div class="modal fade tg-user-modal" tabindex="-1" role="dialog">
				<div class="tg-modal-content">
					<ul class="tg-modaltabs-nav" role="tablist">
						<li role="presentation" class="active"><a href="#tg-signin-formarea" aria-controls="tg-signin-formarea" role="tab" data-toggle="tab">Sign In</a></li>
						<li role="presentation"><a href="#tg-signup-formarea" class="trigger-signup-formarea" aria-controls="tg-signup-formarea" role="tab" data-toggle="tab">Sign Up</a></li>
					</ul>
					<div class="tab-content tg-haslayout">

						<div role="tabpanel" class="tab-pane tg-haslayout active" id="tg-signin-formarea">
							          <form class="tg-form-modal tg-form-signin do-login-form" method="POST" action="header.php">
                                        <fieldset>
                                            <div class="form-group">
                                                <input type="text" name="user" placeholder="User Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control" placeholder="Password">
                                            </div>
                                            <div class="form-group tg-checkbox">
                                                <label>
                                                    <input type="checkbox" class="form-control">
                                                    Remember Me                                                </label>
                                                <a class="tg-forgot-password" href="wp-login3433.php?action=lostpassword&amp;redirect_to=/">
                                                    <i>Forgot Password</i>
                                                    <i class="fa fa-question-circle"></i>
                                                </a>	
                                            </div>
                              					 <button class="tg-btn tg-btn-lg do-login-button" name="login">LOGIN</button>
                                        </fieldset>
                                    </form>
                        </div>


					<div role="tabpanel" class="tab-pane tg-haslayout" id="tg-signup-formarea">
						<form class="tg-form-modal tg-form-signup do-registration-form" method="POST" action="header.php">
								<fieldset>
									<div class="form-group">
										<input type="email" name="c_email" class="form-control" placeholder="Email" required>
									</div>
									<div class="form-group">
										<input type="text" name="c_name" placeholder="Name" class="form-control" required>
									</div>
									<div class="form-group">
										<input type="text" name="c_contact" class="form-control" placeholder="Phone Number" required>
									</div>
									<div class="form-group">
										<input type="password" name="c_pass" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group tg-checkbox">
									<input name="terms"  type="hidden" value="0" />
										<label>
                                        	<input name="terms" class="form-control" type="checkbox" required>
										 I agree with the terms and conditions                                                                                    </label>
									</div>
                                                                        
									<button class="tg-btn tg-btn-lg  do-register-button" name="register">Create an Account</button>
								</fieldset>
							</form>
                            						</div>
					</div>
				</div>
			</div>
                    <div class="modal fade tg-user-lp-model" tabindex="-1" role="dialog">
            <div class="tg-modal-content">
                <div class="panel-lostps">
                    <form class="tg-form-modal tg-form-signup do-forgot-form">
                        <fieldset>
                            <div class="form-group">
                                <h1>Forgot Password</h1>
                                <p>Forgot your password? Enter the email address for your account to reset your password, otherwise you can<a href="javascript:;" class="try-again-lp">&nbsp;try again</a></p>
                                <div class="forgot-fields">
                                    <div class="form-group">
                                        <input type="email" name="psemail" class="form-control psemail" placeholder="Email Address*">
                                        <input type="hidden" name="tg_pwd_nonce" value="3aec02d3d6" />
                                    </div>
                                </div>
                                       <button class="tg-btn tg-btn-lg  do-lp-button" type="button">Submit</button>
                            </div>
                        </fieldset>
                    </form>    
                </div>
            </div>
        </div>
        		</div>
        	<div style="display:none">
	</div>

<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scriptsef15.js?ver=4.8'></script>
<script type='text/javascript' src='wp-content/plugins/docdirect_core/js/functions7793.js?ver=2.4'></script>
<script type='text/javascript' src='s0.wp.com/wp-content/js/devicepx-jetpack7023.js?ver=201735'></script>
<script type='text/javascript' src='s.gravatar.com/js/gprofilesc01f.js?ver=2017Sepaa'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/jetpack/modules/wpgrohoa288.js?ver=4.8.1'></script>
<script type='text/javascript' src='wp-content/themes/task/js/vendor/bootstrap.min5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/sortable.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/underscore.min4511.js?ver=1.8.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-includes/js/wp-util.mina288.js?ver=4.8.1'></script>
<script type='text/javascript'>

</script>
<script type='text/javascript' src='wp-content/themes/task/js/docdirect_functions5156.js?ver=5.5'></script>
<script type='text/javascript'>
</script>
<script type='text/javascript' src='wp-content/themes/task/js/user_profile5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/moment5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/bookings5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/parallax5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/owl.carousel.min5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/prettyPhoto5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/datetimepicker5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/appear5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/countTo5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/sticky_message5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/vendor/modernizr-2.8.3-respond-1.4.2.min5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/chosen.jquery5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/plugins/unyson/framework/extensions/shortcodes/shortcodes/section/static/js/jquery.fs.wallpapera288.js?ver=4.8.1'></script>
<script type='text/javascript' src='wp-content/plugins/unyson/framework/extensions/shortcodes/shortcodes/section/static/js/scriptsa288.js?ver=4.8.1'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.mina288.js?ver=4.8.1'></script>
<script type='text/javascript' src='wp-content/themes/task/js/map/markerclusterer.min5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/map/infobox5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/map/map5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/map/oms.min5156.js?ver=5.5'></script>
<script type='text/javascript' src='wp-content/themes/task/js/docdir_mapsa288.js?ver=4.8.1'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/position.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/menu.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/wp-a11y.mina288.js?ver=4.8.1'></script>
<script type='text/javascript'>

</script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/autocomplete.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-content/themes/task/js/gmap3.mina288.js?ver=4.8.1'></script>
<script type='text/javascript' src='stats.wp.com/e-201735.js' async defer></script>

</body>
</html>