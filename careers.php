<?php

include "header.php";

?>


                			<div id="tg-innerbanner" class="tg-innerbanner tg-bglight tg-haslayout">
				                <div class="tg-pagebar tg-haslayout opacity-false">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
                                <h1>Careers</h1>
                                							</div>
						</div>
					</div>
				</div>
			</div>
		             	<main id="main" class="tg-page-wrapper tg-haslayout">
            			<div class="container">
				<div class="row">
					<div class="main-page-wrapper tg-haslayout"><div class="fw-page-builder-content">    <section  class="tg-main-section haslayout  stretch_section_contents_corner stretch_section stretch_data"  style="margin-top:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-12 builder-column " >
    
<figure class="frame-img"><img src="../wp-content/uploads/2016/02/Task-About-1400x300.jpg" alt="Image Frame" /></figure>
</div>
        </div>
    </section>
    <div class="section-current-width"></div>    <section  class="tg-main-section haslayout  default"  style="margin-top:-100px;margin-bottom:-100px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12 col-md-2 builder-column " >
    </div>
<div class="col-xs-12 col-md-7 builder-column " >
    <p>Our vision is to provide top quality home/office services through technology. We aim to mend the bridge between customer and client to enable efficient workforce and completion of services and we’re looking for talented people to help us do just that.</p><p>If you enjoy solving challenging, real-world problems; if you believe technology can be used to improve people’s lives; if you hate sub-standard, unprofessional services; if you like taking ownership and enjoy your independence, then you’re the right person for the job. Send us your resume at <a href="mailto:recruitment@taskbob.com">recruitment [at] taskpap.co.ke</a></p></div>
<div class="col-xs-12 col-md-2 builder-column " >
    </div>
        </div>
    </section>
    </div></div>				</div>
			</div>
					</main>
                           
<?php

include "footer.php";

?>