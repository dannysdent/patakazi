<?php

include "header.php";

?>

                			<div id="tg-innerbanner" class="tg-innerbanner tg-bglight tg-haslayout">
				                <div class="tg-pagebar tg-haslayout opacity-false">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
                                <h1>Contact Us</h1>
                                							</div>
						</div>
					</div>
				</div>
			</div>
		             	<main id="main" class="tg-page-wrapper tg-haslayout">
            			<div class="container">
				<div class="row">
					<div class="main-page-wrapper tg-haslayout"><div class="fw-page-builder-content">   
				
    <div class="section-current-width"></div>    <section  class="tg-main-section haslayout  default"  style="padding-top:0px;padding-bottom:0px;background-repeat:no-repeat;background-position:0% 100%; background-size: cover;"  >
        <div class="builder-items">
                        <div class="col-xs-12  col-md-4 builder-column " >
    
    <div class="raw-html-description">
        <h3>Contact Info</h3>    </div>
<p><i class="fa fa-map-marker"></i> Juja Nairobi Kenya, P.O.BOX 57716-00200 Nairobi,<br /><br >P.O.BOX 57716-00200 Nairobi</p><p><i class="fa fa-phone-square"></i> 0798639630 / 0770712586</p><p><i class="fa fa-envelope"></i> info@patakazi.co.ke</p><p><i class="fa fa-home"></i> http://www.patakazi.co.ke/</p></div>
<div class="col-xs-12 col-md-8 builder-column " >
    <div class="sc-contact-form">
	<div class="tg-refinesearcharea contact_wrap">
				<div class="tg-heading-border tg-small">
			<h2>Contact Us</h2>
		</div>
			
		<form class="form-refinesearch tg-haslayout contact_form">
			<div class="message_contact theme-notification"></div>
			<fieldset>
				<div class="row form-data" data-success="Message Sent." data-error="Message Failed." data-email="info@taskpap.co.ke">
					<input type="hidden" id="security" name="security" value="b8ef1a3a5a" /><input type="hidden" name="_wp_http_referer" value="/contact-us/" />					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" name="subject" placeholder="Subject">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" name="username" placeholder="Name">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<input type="email" class="form-control" name="useremail" placeholder="Email">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" name="phone" placeholder="Phone">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<textarea class="form-control" name="description" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="col-sm-6">
						<button type="submit" class="tg-btn contact_now">Send</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>				
</div>

</div>
        </div>
    </section>
    </div></div>				</div>
			</div>
					</main>
                          

 <?php

include "footer.php";

?>