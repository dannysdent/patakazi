     <div class="row">
                          <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-26">
                                    <figure class="doc-featureimg"> 
                                  	<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                     <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                          <a href="../electrical/evans/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170709_161108-270x270.jpg" alt="Evans Kegoya"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="26" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/evans/index.html" class="list-avatar">Evans&nbsp;Kegoya</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="26"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                        <address>Kikuyu, Waiyaki way, Westlands, Limuru, Zambezi, Kinoo, Loresho, Lavington</address>
                                            </li>
                         <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                     <li class="dynamic-location-26"></li>
                                         </ul>
                                    </div>
                                </div>
                            </div>

                            //start delete
                        <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-25">
                                    <figure class="doc-featureimg"> 
                                        <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../electrical/joshua/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170709_163049-270x270.jpg" alt="Joshua Kitangara"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="25" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/joshua/index.html" class="list-avatar">Joshua&nbsp;Kitangara</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="25"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Kikuyu, Waiyaki way, Westlands, Limuru, Zambezi, Kinoo, Loresho, Lavington</address>
                                            </li>
                           <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                            <li class="dynamic-location-25"></li>
                                             </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-24">
                                    <figure class="doc-featureimg"> 
                                          	<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                             	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                   <a href="../plumbing/jacob/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170705_093106-270x270.jpg" alt="Jacob Mumbo"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="24" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../plumbing/jacob/index.html" class="list-avatar">Jacob&nbsp;Mumbo</a></h2>
                                                                   <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="24"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Nairobi CBD</address>
                                            </li>
                     <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                             <li class="dynamic-location-24"></li>
                                                             </ul>
                                    </div>
                                </div>
                            </div>
                                         <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-23">
                                    <figure class="doc-featureimg"> 
                                              	<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                           	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../plumbing/samuel/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170628_155038-270x270.jpg" alt="Samuel Kerichu"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="23" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../plumbing/samuel/index.html" class="list-avatar">Samuel&nbsp;Kerichu</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="23"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Nairobi CBD</address>
                                            </li>
                                                <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                           <li class="dynamic-location-23"></li>
                                                                 </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-22">
                                    <figure class="doc-featureimg"> 
                                            <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                             <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                    <a href="../electrical/james/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170403_134320-270x270.jpg" alt="James Njuru"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="22" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/james/index.html" class="list-avatar">James&nbsp;Njuru</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="22"><i class="fa fa-thumbs-up"></i>3</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Nairobi CBD</address>
                                            </li>
                                        <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                   <li class="dynamic-location-22"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-21">
                                    <figure class="doc-featureimg"> 
                                        <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                  	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                         <a href="../electrical/edward/index.html" class="list-avatar"><img src="wp-content/uploads/2017/07/IMG_20170706_115436-270x270.jpg" alt="Edward Maina"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="21" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/edward/index.html" class="list-avatar">Edward&nbsp;Maina</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="21"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Nairobi CBD</address>
                                            </li>
                                             <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                             <li class="dynamic-location-21"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-17">
                                    <figure class="doc-featureimg"> 
                                        <a class="doc-featuredicon" href=javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                               <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                        <a href="../electrical/kelvin/index.html" class="list-avatar"><img src="wp-content/uploads/2017/03/IMG-20170306-WA000-270x270.jpg" alt="Kelvin Kagio"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="17" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/kelvin/index.html" class="list-avatar">Kelvin&nbsp;Kagio</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="17"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Lavington, Loresho, Westlands, Waiyaki Way, Limuru, Kinoo, Zambezi, Kikuyu</address>
                                            </li>
                                                 <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                    <li class="dynamic-location-17"></li>
                                       </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-14">
                                    <figure class="doc-featureimg"> 
                                  <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                             <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                      <a href="../electronics-technician/timothy/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/IMG_20170225_150614-270x270.jpg" alt="Timothy Okello"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="14" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electronics-technician/timothy/index.html" class="list-avatar">Timothy&nbsp;Okello</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="14"><i class="fa fa-thumbs-up"></i>1</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Nairobi, CBD</address>
                                            </li>
                                 <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                   <li class="dynamic-location-14"></li>
                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-13">
                                    <figure class="doc-featureimg"> 
                        	<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                           	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                          <a href="../painting/steve/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/IMG_20170223_152333-270x270.jpg" alt="Steve Mwaniki"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="13" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../painting/steve/index.html" class="list-avatar">Steve&nbsp;Mwaniki</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="13"><i class="fa fa-thumbs-up"></i>0</a></li>
                                        </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Waiyaki Way, Lavington, Loresho, Kinoo, Westlands, Zambezi, Limuru, Kikuyu</address>
                                            </li>
                                       <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                           <li class="dynamic-location-13"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-12">
                                    <figure class="doc-featureimg"> 
                        <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                         	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../electrical/lawrence/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/IMG_20170223_133510-1-270x270.jpg" alt="Lawrence Njau"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="12" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/lawrence/index.html" class="list-avatar">Lawrence&nbsp;Njau</a></h2>
                              <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="12"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                   <li> <i class="fa fa-map-marker"></i>
                                              <address>Lavington, Loresho, Westlands, Waiyaki Way, Limuru, Kinoo, Zambezi, Kikuyu</address>
                                            </li>
                                        <li><i class="fa fa-phone"></i><span>0798639630 / 0770712586</span></li>
                                                                                                                            
                                     <li class="dynamic-location-12"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-11">
                                    <figure class="doc-featureimg"> 
                              		<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                     	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../plumbing/andrew/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/IMG_20170223_152910-270x270.jpg" alt="Andrew Mungai"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="11" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../plumbing/andrew/index.html" class="list-avatar">Andrew&nbsp;Mungai</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="11"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Lavington, Loresho, Westlands, Waiyaki Way, Limuru, Kinoo, Zambezi, Kikuyu</address>
                                            </li>
                                                   <li><i class="fa fa-phone"></i><span>0798639630 /0770712586</span></li>
                                                                                                                            
                               <li class="dynamic-location-11"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-10">
                                    <figure class="doc-featureimg"> 
                                <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                           	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../plumbing/joseph/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/Joseph-Muiruri-270x270.jpg" alt="Joseph Muiruri"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="10" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../plumbing/joseph/index.html" class="list-avatar">Joseph&nbsp;Muiruri</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="10"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Lavington, Loresho, Westlands, Waiyaki Way, Limuru, Kinoo, Zambezi, Kikuyu</address>
                                            </li>
                                        <li><i class="fa fa-phone"></i><span>0798639630 /0770712586</span></li>
                            
                                            <li class="dynamic-location-10"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-9">
                                    <figure class="doc-featureimg"> 
                                      <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                     <a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                           <a href="../carpentry/moses/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/Moses-Gitau-270x270.jpg" alt="Moses Gitau"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="9" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../carpentry/moses/index.html" class="list-avatar">Moses&nbsp;Gitau</a></h2>
                                                         <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="9"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                        </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                              <li> <i class="fa fa-map-marker"></i>
                                              <address>Lavington, Loresho, Westlands, Waiyaki Way, Limuru, Kinoo, Zambezi, Kikuyu</address>
                                            </li>
                                                    <li><i class="fa fa-phone"></i><span>0798639630 /0770712586</span></li>
                                                                                                                            
                                                      <li class="dynamic-location-9"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-8">
                                    <figure class="doc-featureimg"> 
                                                      <a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                                                             	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                                            <a href="../carpentry/makio/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/Makio-Steve-270x270.jpg" alt="Makio Steve"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="8" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../carpentry/makio/index.html" class="list-avatar">Makio&nbsp;Steve</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="8"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Westlands, Ruaka</address>
                                            </li>
                                          <li><i class="fa fa-phone"></i><span>0798639630 / 077071258</span></li>
                                                                                                                            
                                                  <li class="dynamic-location-8"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6  col-xs-6 doc-verticalaligntop">
                            	<div class="doc-featurelist" class="user-7">
                                    <figure class="doc-featureimg"> 
                                              		<a class="doc-featuredicon" href="javascript:;"><i class="fa fa-bolt"></i>featured</a>
			                             	<a class="doc-featuredicon doc-verfiedicon" href="javascript:;"><i class="fa fa-shield"></i>Verified</a>
                                <a href="../electrical/gabael/index.html" class="list-avatar"><img src="wp-content/uploads/2017/02/Gabael-Muriithi-270x270.jpg" alt="Gabael Muriithi"></a>
                                      <figcaption></figcaption>
                                    </figure>
                                    <div class="doc-featurecontent">
                                      <div class="doc-featurehead">
                                        <a data-view_type="v2" class="doc-favoriteicon doc-notfavorite add-to-fav" data-wl_id="7" href="javascript:;"><i class="fa fa-heart"></i></a>                                        <h2><a href="../electrical/gabael/index.html" class="list-avatar">Gabael&nbsp;Muriithi</a></h2>
                                                                                <ul class="doc-matadata">
                                          
                                          <li><a href="javascript:;" class="do-like-me" data-like_id="7"><i class="fa fa-thumbs-up"></i>0</a></li>
                                                                                  </ul>
                                      </div>
                                      <ul class="doc-addressinfo">
                                                                                    <li> <i class="fa fa-map-marker"></i>
                                              <address>Ruai, Utawala, Nairobi, Kenya</address>
                                            </li>
                                       <li><i class="fa fa-phone"></i><span>0798639630 /0770712586</span></li>
                                                      <li class="dynamic-location-7"></li>
                                                                                                                              </ul>
                                    </div>
                                </div>
                            </div>
                     
                    </div>
